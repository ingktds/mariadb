FROM centos:latest
LABEL maintainer="tadashi.1027@gmail.com"
ENV TZ=Asia/Tokyo
RUN yum -y install mariadb-server && \
/usr/libexec/mariadb-prepare-db-dir /var/lib/mysql
EXPOSE 3306
CMD ["/usr/bin/mysqld_safe"]
