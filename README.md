# Docker Image for MariaDB

# Build docker image

```
$ cd [WORK_DIR]/mariadb
$ docker build -t <container_image_name> .
```

# Run docker image

```
$ cd [WORK_DIR]/mariadb
$ docker run --rm -d -p 3306 -v [WORK_DIR]/mariadb/my.cnf:/etc/my.cnf --name mariadb <container_image_name>
$ docker exec -it <container_id> /usr/bin/mysql_secure_installation
$ docker exec -it <container_id> mysql -u root -p
```
